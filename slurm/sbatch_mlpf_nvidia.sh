#!/bin/sh

#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1

#SBATCH --partition=gpu
#SBATCH --cpus-per-task=64
#SBATCH --gpus=4
#SBATCH --gpus-per-task=4
#SBATCH --constraint=a100,sxm4

#SBATCH --job-name=mlpf_nvidia

#SBATCH --output=logs/%x_%j.out
#SBATCH --error=logs/%x_%j.err


# Add jobscript to job output
echo "#################### Job submission script. #############################"
cat $0
echo "################# End of job submission script. #########################"


# Load dependencies
module purge
module load gcc cuda/11.1.0_455.23.05 cuda/11.4.2 cudnn/8.2.4.15-11.4 singularity

# Print GPU info
nvidia-smi

# Run benchmark container
singularity run -c -B /mnt/home/$USER/mlpf/nvidia:/results -B /tmp --nv /mnt/home/$USER/mlpf/mlpf_main.sif --nepochs 10 --batch_size 10 --num_devices 10
